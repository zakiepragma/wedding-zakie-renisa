import React from 'react'

function Organization () {
  return (
    <div id='organization' className='organization section-padding bg-pink'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 mb-30'>
            <span className='oliven-title-meta'>Wedding</span>
            <h2 className='oliven-title'>Qolallahu wa Qola Rosulullah</h2>
          </div>
        </div>
        <div className='row bord-box bg-img' data-background='images/wedding1.png'>
          <div className='col-md-6 item-box'>
            <h2 className='custom-font numb'>01</h2>
            <h6 className='title'>Ayat Al-Qur'an</h6>
            <p>
            وَمِنْ آيَاتِهِ أَنْ خَلَقَ لَكُمْ مِنْ أَنْفُسِكُمْ أَزْوَاجًا لِتَسْكُنُوا إِلَيْهَا وَجَعَلَ بَيْنَكُمْ مَوَدَّةً وَرَحْمَةً ۚ إِنَّ فِي ذَٰلِكَ لَآيَاتٍ لِقَوْمٍ يَتَفَكَّرُونَ
            </p>
            <p>
            “Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu
            isteri-isteri dari jenismu sendiri, supaya kamu cenderung dan merasa tenteram
            kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang. Sesungguhnya pada
            yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berfikir.”
            [QS. Ar Rum: 21].
            </p>
            <p>
            Baca selengkapnya https://muslimah.or.id/9281-untukmu-yang-berbahagia-bag-i.html
            </p>
          </div>
          <div className='col-md-6 item-box'>
            <h2 className='custom-font numb'>02</h2>
            <h6 className='title'>Hadist Nabi</h6>
            <p>
            Rasulullah shallallahu ‘alaihi wa sallam bersabda,

يَا مَعْشَرَ الشَّبَابِ مَنِ اسْتَطَاعَ مِنْكُمُ الْبَاءَةَ فَلْيَتَزَوَّجْ فَإِنَّهُ أَغَضُّ لِلْبَصَرِ وَأَحْصَنُ لِلْفَرْجِ وَمَنْ لَمْ يَسْتَطِعْ فَعَلَيْهِ بِالصَّوْمِ فَإِنَّهُ لَهُ وِجَاءٌ
            </p>
            <p>
            “Wahai para pemuda, barangsiapa yang memiliki baa-ah , maka menikahlah.
            Karena itu lebih akan menundukkan pandangan dan lebih menjaga kemaluan.
            Barangsiapa yang belum mampu, maka berpuasalah karena puasa itu bagai obat
            pengekang baginya.”
            (HR. Bukhari no. 5065 dan Muslim no. 1400).
            </p>
            <p>
            Sumber https://rumaysho.com/2723-hukum-menikah.html
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Organization
