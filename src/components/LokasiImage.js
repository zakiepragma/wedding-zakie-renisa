// 0.5382489290253645, 101.40135942588722        
import React from 'react';

function LokasiImage() {
  return (
    <div id='lokasi'>
      <a href='https://goo.gl/maps/cwZ8GpbyRS5EViQB9' target="_blank" without rel="noreferrer">
        <img src={require('../assets/images/lokasi-image.png')} />
      </a>
    </div>
  );
}

export default LokasiImage;