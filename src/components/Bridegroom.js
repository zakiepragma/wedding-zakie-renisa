import React from 'react'
import Bride from '../assets/images/akhwat.jpeg'
import Groom from '../assets/images/ikhwan.png'
function Bridegroom () {
  return (
    <div id='couple' className='bridegroom clear section-padding bg-pink'>
      <div className='container'>
      <div className='row mb-50'>
          <div
            className='col-md-12 text-center animate-box'
            data-animate-effect='fadeInUp'
          >
            <h3 className='oliven-couple-title'>💍 *Undangan Walimatul Ursy*💍</h3>
            <h4 className='oliven-couple-subtitle'>
            <p>بسم الله الرحمن الرحيم</p>
            <p>
              السلام عليكم و رحمة الله و بركاته
            </p>
            <p>💐Dengan memohon Rahmat & Ridha Allah, dan tidak mengurangi rasa hormat.
              Izinkan kami mengundang Saudara/i sekalian untuk hadir dalam
              acara pernikahan kami
            </p>
            <p>
              Doa untuk pengantin
            بَارَكَ اللَّهُ لَكَ وَبَارَكَ عَلَيْكَ وَجَمَعَ بَيْنَكُمَا فِى خَيْرٍ
            </p>
            <p>
            _(Baarakallahu laka wa baraka ‘alaika wa jama’a bainakuma fii khayr)_
            </p>
            <p>
            “Semoga Allah memberkahimu ketika bahagia dan ketika susah dan mengumpulkan kalian berdua dalam kebaikan.”
            </p>
            </h4>
          </div>
        </div>
        <div className='row mb-60'>
          <div className='col-md-6'>
            <div
              className='item toright mb-30 animate-box'
              data-animate-effect='fadeInLeft'
            >
              <div className='img'>
                {' '}
                <img src={Bride} alt='' />{' '}
              </div>
              <div className='info valign'>
                <div className='full-width'>
                  <h6>
                    Renisa Nurayu Nastiti <i className='ti-heart'></i>
                  </h6>{' '}
                  <span>21 Tahun</span>
                  <p>
                    Anak ke-4 dari Bapak Nasrun dan Ibu Titin.
                  </p>
                  <div className='social'>
                    <div className='full-width'>
                      <a href='#0' className='icon'>
                        {' '}
                        <i className='ti-facebook'></i>{' '}
                      </a>
                      <a href='#0' className='icon'>
                        {' '}
                        <i className='ti-twitter'></i>{' '}
                      </a>
                      <a href='#0' className='icon'>
                        {' '}
                        <i className='ti-instagram'></i>{' '}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-md-6'>
            <div
              className='item mb-30 animate-box'
              data-animate-effect='fadeInRight'
            >
              <div className='img'>
                {' '}
                <img src={Groom} alt='' />{' '}
              </div>
              <div className='info valign'>
                <div className='full-width'>
                  <h6>
                    Muhammad Zakie, S.T <i className='ti-heart'></i>
                  </h6>{' '}
                  <span>24 Tahun</span>
                  <p>
                    Anak pertama dari Bapak Khairul dan Ibu Zirda.
                  </p>
                  <div className='social'>
                    <div className='full-width'>
                      <a href='#0' className='icon'>
                        {' '}
                        <i className='ti-facebook'></i>{' '}
                      </a>
                      <a href='#0' className='icon'>
                        {' '}
                        <i className='ti-twitter'></i>{' '}
                      </a>
                      <a href='#0' className='icon'>
                        {' '}
                        <i className='ti-instagram'></i>{' '}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Bridegroom
