import React from 'react'

function Footer () {
  return (
    <div className='footer2'>
      <div className='oliven-narrow-content'>
        <div className='row'>
          <div className='col-md-12 text-center'>
            <h2>
              <a href='/'>
                <span>
                  Renisa <small>&</small> Zakie
                </span>
              </a>
            </h2>
            <p className='copyright'>November 6, 2022 – Pekanbaru, Riau</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer