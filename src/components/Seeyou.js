import React from 'react'

function Seeyou () {
  return (
    <div
      id='seeyou'
      className='seeyou section-padding bg-img bg-fixed'
    >
      <div className='container'>
        <div className='row'>
          <div className='section-head col-md-12 text-center'>
            <span>
              <i className='ti-heart'></i>
            </span>
            <h4>Berharap kita bisa bertemu di walimatul ursy kami!</h4>
            <h3>Ahad, 06.11.2022</h3>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Seeyou
