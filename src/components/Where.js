import React from 'react'
import where1 from '../assets/images/nikah1.jpg'
import where2 from '../assets/images/nikah2.jpg'
import where3 from '../assets/images/logo-mi.jpg'
function Where () {
  return (
    <div id='whenwhere' className='whenwhere section-padding bg-pink'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 mb-30'>
            {' '}
            <span className='oliven-title-meta'>Questions</span>
            <h2 className='oliven-title'>Kapan & Dimana</h2>{' '}
          </div>
        </div>
        <div className='row'>
          <div className='item col-12 col-md-4'>
            <div className='whenwhere-img'>
              {' '}
              <img src={where3} alt='' />
            </div>
            <div className='content'>
              <h5>Ta'aruf</h5>
              <p>
                <i className='ti-location-pin'></i>
                Mawaddah Indoenesia https://mawaddahindonesia.com/
              </p>
              <p>
                <i className='ti-time'></i> <span>Sabtu, 20 Agustus 2022</span>
              </p>
            </div>
          </div>
          <div className='item col-12 col-md-4'>
            <div className='whenwhere-img'>
              {' '}
              <img src={where1} alt='' />
            </div>
            <div className='content'>
              <h5>Akad Nikah</h5>
              <a href='https://goo.gl/maps/mgVYhqVNt25vcH427' target="_blank" without rel="noreferrer">
              <p>
                <i className='ti-location-pin'></i>
                KUA Payung Sekaki, Kota Pekanbaru, Riau 28292
              </p>
              <p>
                <i className='ti-time'></i> <span>Jum'at, 4 Nov 2022 - 09:00 WIB</span>
              </p>
              </a>
            </div>
          </div>
          <div className='item col-12 col-md-4'>
            <div className='whenwhere-img'>
              {' '}
              <img src={where2} alt='' />
            </div>
            <div className='content'>
              <h5>Walimatul Ursy</h5>
              <a href='https://goo.gl/maps/cwZ8GpbyRS5EViQB9' target="_blank" without rel="noreferrer">
              <p>
                <i className='ti-direction-alt'></i>
                Jl. Siak II, Gg. Surya Indah Samping SPBU, Tampan, Kec. Payung Sekaki, Kota Pekanbaru
              </p>
              <p>
                <i className='ti-time'></i> <span>Ahad, 6 Nov 2022 - (11:00 – Selesai)</span>
              </p>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Where
