// 0.5382489290253645, 101.40135942588722        
import React from 'react';
import L from 'leaflet';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import '../App.css';
import 'leaflet/dist/leaflet.css';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('../assets/images/marker-icon-2x.png'),
  iconUrl: require('../assets/images/marker-icon.png'),
  shadowUrl: require('../assets/images/marker-shadow.png')
});

const location = [0.5382489290253645, 101.40135942588722];
const zoom = 17;

function Lokasi() {
  return (
    <div id='lokasi' className='App'>
      <div className='bingkai'>
        <h4 className='oliven-story-subtitle'>Lokasi Walimatul Ursy</h4>
        <h3 className='oliven-story-title'>Kediaman Mempelai Wanita</h3>
        <a className='btnMap jarakKebawah' href='https://goo.gl/maps/cwZ8GpbyRS5EViQB9' target="_blank" without rel="noreferrer">Buka Map</a>
      <MapContainer center={location} zoom={zoom} scrollWheelZoom={false}>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution="&copy; <a href=&quot;https://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors" />
        <Marker position={location}>
          <Popup>Lokasi Walimatul Ursy</Popup>
        </Marker>
      </MapContainer>
      </div>
    </div>
  );
}

export default Lokasi;