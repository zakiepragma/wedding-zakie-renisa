import React from 'react'
import storyImage from '../assets/images/ikhwanakhwat1.png'
function Story () {
  return (
    <div id='story' className='story section-padding'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-5 mb-30'>
            <div className='story-img animate-box'>
              <div className='img'>
                {' '}
                <img src={storyImage} className='img-fluid' alt='' />{' '}
              </div>
              <div
                className='story-img-2 story-wedding'
              ></div>
            </div>
          </div>
          <div className='col-md-7 animate-box'>
            <h4 className='oliven-story-subtitle'>Cinta Kita.</h4>
            <h3 className='oliven-story-title'>Cerita Kita</h3>
            <p>
              Berawal dari ta'aruf di https://mawaddahindonesia.com/
              sekarang berganti menjadi https://mawaddati.com/ yang dikelolah
              oleh Ustadz Dr. Khalid Basalamah, Lc MA.
            </p>
            <p>
              Ta'aruf mulai pada 20 Agustus 2022 dan sepekan setelah itu
              langsung datang kerumah akhwat untuk nadzor dan sepekan selanjutnya
              langsung bawa orang tua ke rumah akhwat untuk khitbah.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Story