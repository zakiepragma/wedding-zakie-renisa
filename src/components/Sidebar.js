import React, { useState } from 'react'
function Sidebar () {
  const [show, setShow] = useState(false);

  const openMenu = (e) => {
    e.preventDefault();
    setShow(!show);
    document.body.classList.toggle('slide');
  }

  return (
    <>
      <a href='/' onClick={openMenu} className={`js-oliven-nav-toggle oliven-nav-toggle${show ? ' active': ''}`}>
        <i></i>
      </a>
      <aside id='oliven-aside'>
        <div className='oliven-logo'>
          <a href='/'>
            <span>
              Renisa <small>&</small> Zakie
            </span>
            <h6>04.11.2022</h6>
          </a>
        </div>
        <nav className='oliven-main-menu'>
          <ul>
            <li>
              <a href='#home'>Home</a>
            </li>
            <li>
              <a href='#couple'>Pengantin</a>
            </li>
            <li>
              <a href='#organization'>Qolallahu</a>
            </li>
            <li>
              <a href='#story'>Cerita</a>
            </li>
            <li>
              <a href='#whenwhere'>Kapan & Dimana</a>
            </li>
            <li>
              <a href='#gallery'>Galeri</a>
            </li>
            <li>
              <a href='#lokasi'>Lokasi Walimah</a>
            </li>
            <li>
              <a href='#rsvp'>R.S.V.P</a>
            </li>
          </ul>
        </nav>
        <div className='footer1'>
          {' '}
          <span className='separator'></span>
          <p>
            Renisa & Zakie walimatul ursy
            <br />
            6 November 2022, Pekanbaru
          </p>
          <a href='https://youtube.com/channel/UCfLBG0ean6WnQ1ftTsyMt0w' target="_blank" without rel="noreferrer">@programmercintasunnah</a>
        </div>
      </aside>
    </>
  )
}

export default Sidebar
